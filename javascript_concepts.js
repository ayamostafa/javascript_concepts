var x = 4;
function fun_x() {
    var y = 4;
}
try{
console.log(y); // throw javascript error => y is not defined
}catch (e){
    console.log(e.message)
}
console.log("__________");
// Referenced before declaration
console.log( example ); // Expect output: undefined
var example = 'example';
console.log("__________");
// Referenced before declaration
try{
    console.log( example2 ); //  throw javascript error => can't access example2 before initialization because example2 is not defined
    let example2 = 'example2';
}catch (e){
    console.log(e.message)
}
console.log("__________");
var objKey = 'name'
const person = {
    [objKey]:'aya'
}
console.log(person.name)
console.log("__________");

const varName = 'first';
function computeNameType( type ) {
    return type + 'Name';
}

const person2 = {
    [ varName + 'Name' ]: 'John',
    [ computeNameType( 'last' ) ]: 'Smith'
};

console.log( person2.firstName ); // Expected output: John
console.log( person2.lastName ); // Expected output: Smith
console.log("__________");

let names = [ 'John', 'Michael' ];
let [ name1, name2 ] = names;

console.log( name1 ); // Expected output: 'John'
console.log( name2 ); // Expected output: 'Michael'
console.log("__________");
function fn( num1, num2, ...args ) {
    // Destructures an indefinite number of function parameters into the array args, excluding the first two arguments passed in.
    console.log( num1 );
    console.log( num2 );
    console.log( args );
}

fn( 1, 2, 3, 4, 5, 6 );
// Expected output
// 1
// 2
// [ 3, 4, 5, 6 ]
console.log("__________");
const [ n1, n2, n3, ...remaining ] = [ 1, 2, 3, 4, 5, 6 ];
console.log( n1 ); // Expected output: 1
console.log( n2 ); // Expected output: 2
console.log( n3 ); // Expected output: 3
console.log( remaining ); // Expected output: [ 4, 5, 6 ]

console.log("__________");
const obj = { firstName: 'Bob', lastName: 'Smith' };
const { firstName, lastName } = obj;
console.log( firstName ); // Expected output: 'Bob'
console.log( lastName ); // Expected output: 'Smith'
// console.log("__________");
// const obj2 = { firstName: 'Bob', middleName: 'Chris', lastName: 'Smith' };
// const { firstName, ...otherNames } = obj2;
// console.log( firstName ); // Expected output: 'Bob'
// console.log( otherNames ); // Expected output: { middleName: 'Chris', lastName: 'Smith' }
// console.log("__________");
// const obj3 = { firstName: 'Bob', lastName: 'Smith' };
// const { firstName: first = 'Samantha', middleName: middle = 'Chris' } = obj3;
// console.log( first ); // Expected output: 'Bob'
// console.log( middle); // Expected output: 'Chris'
console.log("__________");
class Human{
    constructor(name,age){
        this.name=age;
        this.age=age;
    }
}
const myH1 = new Human('ali',20);
myH1.age = 38;
console.log("__________");
console.log("javascript class inheritance");
class House {
    constructor( address = 'somewhere' ) {
        this.address = address;
    }
}
class Mansion extends House {
    constructor( address, floors ) {
        super( address );
        this.floors = floors;
    }
}
let mansion = new Mansion( 'Hollywood CA, USA', 6, 'Brad Pitt' );
console.log( mansion.floors ); // Expected output: 6
console.log("__________");
console.log("___export default_______");
export default function (){}
console.log("___import modules in html file_______");
export const PI = 3.1415; //javascript file "ex: index.js"
// <script type='module'>
//     import * as ModuleExample from './index.js';
//     console.log(ModuleExample.PI)
// </script>

console.log("___ generator function ____");
function generator(arr){
    let currentIdx=0;
    return {
        next(){
            return  currentIdx < arr.length ? {value:arr[currentIdx++],done:false}:{done:true}
        }
    }
}

let gen = generator([1,2,3]);
x = gen.next();
x = gen.next();
x = gen.next();
x = gen.next();
console.log(x);
